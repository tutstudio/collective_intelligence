<?php
$critics = [
	'Lisa Rose' => [
		'Lady in the water' => 2.5,
		'Snakes on Plane' => 3.5,
		'Just My Luck' => 3.0,
		'Superman Returns' => 3.5,
		'You, Me and Dupree' => 2.5,
		'The Night Listener' => 3.0
	],
	'Gene Seymour' => [
		'Lady in the water' => 3.0,
		'Snakes on Plane' => 3.5,
		'Just My Luck' => 1.5,
		'Superman Returns' => 5.0,
		'You, Me and Dupree' => 3.5,
		'The Night Listener' => 3.0
	],
	'Michael Phillips' => [
		'Lady in the water' => 2.5,
		'Snakes on Plane' => 3.0,
		'Superman Returns' => 3.5,
		'The Night Listener' => 4.0
	],
	'Claudia Puig' => [
		'Snakes on Plane' => 3.5,
		'Just My Luck' => 3.0,
		'Superman Returns' => 4.0,
		'You, Me and Dupree' => 2.5,
		'The Night Listener' => 4.5
	],
	'Mick LaSalle' => [
		'Lady in the water' => 3.0,
		'Snakes on Plane' => 4.0,
		'Just My Luck' => 2.0,
		'Superman Returns' => 3.0,
		'You, Me and Dupree' => 2.0,
		'The Night Listener' => 3.0
	],
	'Jack Matthews' => [
		'Lady in the water' => 3.0,
		'Snakes on Plane' => 4.0,
		'Just My Luck' => 3.0,
		'Superman Returns' => 5.0,
		'You, Me and Dupree' => 3.5,
		'The Night Listener' => 3.0
	],
	'Toby' => [
		'Snakes on a Plane' => 4.5,
		'You, Me and Dupree' => 1.0,
		'Superman Returns' => 4.0
	]
];

function sim_distance($prefs, $person1, $person2) {
	$si = [];
	$pow = [];

	foreach ($prefs[$person1] as $item => $value) {
		if (array_key_exists($item, $prefs[$person2])) {
			$si[$item] = 1;
			$pow[] = pow(($prefs[$person1][$item]-$prefs[$person2][$item]),2);
		}
	}
	if (count($si) == 0) {
		return 0;
	}


	$sum_of_squares = array_sum($pow);

	return 1/(1+$sum_of_squares);
}

// Returns the Pearson correlation coefficient for p1 and p2
function sim_pearson($prefs, $p1, $p2) {
	$si = [];
	foreach ($prefs[$p1] as $key => $value) {
		if (array_key_exists($key, $prefs[$p2])) {
			$si[$key] = 1;
		}
	}

	$n = count($si);

	if ($n == 0) {
		return 0;
	}

	foreach ($si as $key => $value) {
		if (array_key_exists($key, $prefs[$p1])) {
			$p1Array[] = $prefs[$p1][$key];
			$p1ArraySq[] = pow($prefs[$p1][$key],2);
		}
		if (array_key_exists($key, $prefs[$p2])) {
			$p2Array[] = $prefs[$p2][$key];
			$p2ArraySq[] = pow($prefs[$p2][$key],2);
		}
		if (array_key_exists($key, $prefs[$p1]) && array_key_exists($key, $prefs[$p2])) {
			$pSumArray[] = $prefs[$p1][$key] * $prefs[$p2][$key]; 
		}
	}

	$sum1 = array_sum($p1Array);
	$sum2 = array_sum($p2Array);

	$sum1Sq = array_sum($p1ArraySq);
	$sum2Sq = array_sum($p2ArraySq);

	$pSum = array_sum($pSumArray);

	$num = $pSum - ($sum1 * $sum2 / $n);

	$den = sqrt(($sum1Sq - pow($sum1,2)/$n)*($sum2Sq-pow($sum2,2)/$n));

	if ($den == 0) {
		return 0;
	}

	$r = $num / $den;

	return (double) $r;
}

function topMatches($prefs, $person, $n=5, $similarity='sim_pearson') {
	foreach ($prefs as $key => $value) {
		if ($person !== $key) {
			$scores[$key] = $similarity($prefs, $person, $key);
		}
	}

	arsort($scores);

	return array_slice($scores, 0, $n) ;
}

//Gets recommendations for a user by using weighted average of other user's rankings
function getRecommendations($prefs, $person, $similarity='sim_pearson') {
	$totals = [];
	$simSum = [];
	$rankings = [];

	foreach ($prefs as $name => $value) {
		if ($name == $person) {
			continue;
		}
		$sim = $similarity($prefs, $person, $name);

		if ($sim <= 0) {
			continue;
		}

		foreach ($prefs[$name] as $item => $value) {
			if (!array_key_exists($item, $prefs[$person]) || $prefs[$person][$item]==0) {
				$totals[$item] = isset($totals[$item]) ? $totals[$item] : 0;
				$totals[$item] += $prefs[$name][$item] * $sim;

				$simSum[$item] = isset($simSum[$item]) ? $simSum[$item] : 0;
				$simSum[$item] += $sim;
			}
		}
	}

	foreach ($totals as $key => $value) {
		$rankings[$key] = (float) ($value/$simSum[$key]);
	}

	arsort($rankings);

	return $rankings;
}

$result = sim_distance($critics,'Lisa Rose','Gene Seymour');

 echo 'Distance betwin Lisa Rose and Gene Seymour is '.$result."\n";

 $result2 = sim_pearson($critics,'Lisa Rose','Gene Seymour');

 echo 'Coorelance betwin Lisa Rose and Gene Seymour is '.$result2."\n";

 $matches = topMatches($critics, 'Toby', 3);

 print 'Matches by pearson: '.json_encode($matches)."\n";

 $matches2 = topMatches($critics, 'Toby', 6, 'sim_distance');

 print 'Matches by Euclidean: '.json_encode($matches2)."\n";

 $matches3 = getRecommendations($critics, 'Toby');

 print 'Recommendations by Pearson: '.json_encode($matches3)."\n";

 $matches4 = getRecommendations($critics, 'Toby', 'sim_distance');

 print 'recommendations by Euclidean: '.json_encode($matches4)."\n";
?>